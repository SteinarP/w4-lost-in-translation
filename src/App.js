import React from "react";
import "./App.css";

import StartPage from "./containers/StartPage";
import TranslatePage from "./containers/TranslatePage";
import ProfilePage from "./containers/ProfilePage";
import NotFoundPage from "./containers/NotFoundPage";

//import {StartPage, TranslatePage, ProfilePage, NotFoundPage} from "./containers"

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
} from "react-router-dom";

function App() {
  const navBar = (
    <nav>
      <Link to="/">Home |</Link>
      <Link to="/login">Login |</Link>
      <Link to="/translate">Translate |</Link>
      <Link to="/profile">Profile</Link>
    </nav>
  );

  const routing = (
    <Router>
    {navBar}
      <Switch>
        <Route exact path="/">
          <Redirect to="/login" />
        </Route>
        <Route path="/login" component={StartPage} />
        <Route path="/translate" component={TranslatePage} />
        <Route path="/profile" component={ProfilePage} />
        <Route path="*" component={NotFoundPage} />
      </Switch>
    </Router>
  );

  return (
    <div className="App">
      <h3>Lost in Translation</h3>
      <hr/>
      {routing}
    </div>
  );
}

export default App;
