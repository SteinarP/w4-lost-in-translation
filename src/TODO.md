# TODO
## Startup page
-[x] Enter name in field
-[X] Click: Store name (local|session)
-[X] Once saved, go to translation page
## Translation page
-[X] Enter text in field
-[X] Click "translate": Trigger method
-[X] Show signs in box below
-[X] Store 10 last translations (text)
## Profile page
-[X] Page with username
-[X] Display 10 last translations
-[ ] Button clearing 10 and logout
-[ ] Logout --> Start page
