import React, { useState } from 'react';
import './App.css';

function App() {

  const [ newFruit, setNewFruit ] = useState("");
  const title = "Welcome to the MovieBox";
  const items = ["Banana 🍌", "Apple 🍎", "Grapes 🍇", "Orange 🍊"];

  const onNewFruitChanged = (event) => {
    console.log(event.target.value);
    //Super illegal: newFruit = event.target.value   (USE SETTER! Never directly set value!)
    setNewFruit(event.target.value.trim());
  };

  const fruitList  = items.map(fruit => {
    return <li key={fruit}>{ fruit }</li>
  });

  return (
    <div className="App">
      <h3>{title}</h3>

      <input type="text" placeholder="Add new fruit" onChange={onNewFruitChanged}></input>
      {newFruit}

      <ul>{fruitList}</ul>
    </div>
  );
}

export default App;
