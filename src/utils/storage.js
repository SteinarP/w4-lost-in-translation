// Local storage stays in browser until cleared. Don't use for sensitive data.
export const setStorage = (key, value) => {
    localStorage.setItem(key, JSON.stringify(value));
}

export const getStorage = key => {
    const storedValue = localStorage.getItem(key);
    console.log("Returning",key,"parsed:", JSON.parse(storedValue));
    if (!storedValue || storedValue===undefined){
        return false;
    }
    return JSON.parse(storedValue);
}

export const clearStorage = () => {
    localStorage.clear();
}

export const pushLimitedStorage = (arrayKey, value, limit=10) => {
    const current = getStorage(arrayKey);
    if (!current ){
        setStorage(arrayKey, [value]);
        return;
    }
    let arrayEdit = current.slice(); //clone
    arrayEdit.push(value);

    while (arrayEdit.length > limit){
        arrayEdit.shift();
    }
    setStorage(arrayKey, arrayEdit);
}