import React from "react";

const SpriteGallery = (props) => {
  if (!props.text || props.text.length === 0) {
    return <div>null</div>;
  }
  console.log("Signing text: ", props.text);
  let charsToSign = props.text.toUpperCase().split("");

  const ALPHA = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; //Valid signs
  const spriteList = charsToSign.map((char, index) => {
    if (!ALPHA.includes(char)) {
      return (
        <div className="image-frame" key={index}>
          <p className="">{char}</p>
        </div>
      );
    }

    return (
      <div className="image-frame" key={index}>
        <img
          key={index}
          className="sign"
          width="100%"
          alt={`ASL ${char}`}
          title={`${char}`}
          src={require(`../res/signs/${char.toLowerCase()}.png`)}
        />
        <span>{char}</span>
      </div>
    );
  });

  //
  return <div className="box">{spriteList}</div>;
};

export default SpriteGallery;
