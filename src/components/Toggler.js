import React from "react";
import "./Toggler.css";

const Toggler = (props) => {
    return (
      <div>
        <label className="switch">
        <input type="checkbox" onChange={props.onChange}/>
        <span className="slider round"></span>
        </label>
        <span className="sliderText">{props.text}</span>
      </div>
    )
}

export default Toggler;