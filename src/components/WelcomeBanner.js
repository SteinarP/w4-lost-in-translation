import logo from "../res/Logo-Hello.png";

import React from "react";

const WelcomeBanner = (props) => {
  //Default props
  const title = props.title ? props.title : "Lost in Translation";
  const subtitle = props.subtitle ? props.subtitle : "Get started";

  return (
    <div className="flex-container">
      <div>
        <img src={logo} className="App-logo" alt="logo" />
      </div>
      <div>
        <h1>{title}</h1>
        <h3>{subtitle}</h3>
      </div>
    </div>
  );
};

export default WelcomeBanner;
