import React from "react";
import SpriteGallery from "./SpriteGallery";

const SpriteGalleryList = (props) => {
    if (!props.strings || props.strings.length===0) {
      return <div>null</div>;
    }
    console.log("Signing array of",props.length,"length");
    const idk = (stringArray) => (<ul>
      {stringArray.map((e,i) => 
        <li>
          <SpriteGallery text={e} key={i}/>
        </li>
        )}
    </ul>);

    return (
        <div>
        Starting list.
        {idk(props.strings)}
        Listing done.
        </div>
    )
}

export default SpriteGalleryList;
