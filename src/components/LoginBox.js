import React, { useState } from "react";
import TextInputBox from "./TextInputBox";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
} from "react-router-dom";
import { setStorage, getStorage } from "../utils/storage";

const LoginBox = (props) => {
  const [username, setUsername] = useState("user");

  const onUsernameChanged = (event) => {
    console.log(event.target.value);
    setUsername(event.target.value.trim());
  };

  const onUsernameSubmit = (event) => {
    console.log(event.target.value);
    console.log("Submitting username: ", {username});
    setStorage("username", username);
    console.log("Wrote to storage:", getStorage("username"));
  };

  return (
    <div>
      <TextInputBox
        placeholder={"What's your name?"}
        onChange={onUsernameChanged}
        onSubmit={onUsernameSubmit}
        routeLink={"/translate"}
      />
    </div>
  );
};

export default LoginBox;
