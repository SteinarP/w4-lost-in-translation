import React from "react";
import keyboard from "../res/icon_keyboard.png";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
} from "react-router-dom";
//import SpriteGallery from "./SpriteGallery";

const TextInputBox = (props) => {
  
  //For some strange reason I can't manage to edit button's CSS
  //from external file, so forcing it in here instead.
  let btnStyle = {
    background: "#845Ec2", 
    height: "100%",
    color: "#EFEFEF",
    "borderRadius": "50%",
    display:"inline",
  }

  return (
    <div>
      <div className="input-container">
        <img
          src={keyboard}
          height="100%"
          className="left"
          alt="keyboard icon"
        />
        <form>
          <input
            type="text"
            size={props.placeholder.length-4}
            placeholder={props.placeholder}
            onChange={props.onChange}
          />
          <Link to={props.routeLink}>
          <button 
            type="submit" 
            className="btn plswork" 
            onClick={props.onSubmit}
            style={btnStyle}>
            🡲
          </button>
          </Link>
        </form>
      </div>
    </div>
  );
};

export default TextInputBox;
