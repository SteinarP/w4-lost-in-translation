/**
 * Note: Handler can live in parent component and then be
 * passed down to child for event to execute.
 */
import React from "react";
import WelcomeBanner from "../components/WelcomeBanner";
import LoginBox from "../components/LoginBox";

const StartPage = (props) => {
  return (
    <div>
        <WelcomeBanner/>
        <LoginBox/>
    </div>
  );
};

export default StartPage;
