import React, { useState } from "react";
import SpriteGallery from "../components/SpriteGallery";
import { getStorage, setStorage, pushLimitedStorage } from "../utils/storage";
import TextInputBox from "../components/TextInputBox";
import Toggler from "../components/Toggler";
import { useHistory } from "react-router-dom";

const TranslatePage = (props) => {  
  const [text, setText] = useState();
  const [output, setOutput] = useState();
  const [live, setLive] = useState(false);

  const onTextChange = (event) => {
    console.log(live + ": " + event.target.value);
    setText(event.target.value.trim());
    if (live) {
      setOutput(text);
    }
  };

  const onLiveChange = (event) => {
    setLive(event.target.checked);
  };

  const onTextSubmit = (event) => {
    console.log("Last translation:", getStorage("last"));
    console.log("Translating:", { text }, "for user", getStorage("username"));
    setOutput(text);
    setStorage("last", text);

    pushLimitedStorage("recent", text);
    console.log("Current recent:", getStorage("recent"));
  };

  return (
    <div>
      Welcome back, <span>{getStorage("username")}</span>!
      <TextInputBox
        placeholder={"Translate to American Sign Language"}
        onChange={onTextChange}
        onSubmit={onTextSubmit}
        routeLink={"/translate"}
      />
      <Toggler text="Live" onChange={onLiveChange} />
      <SpriteGallery text={output} />
    </div>
  );
};

export default TranslatePage;
