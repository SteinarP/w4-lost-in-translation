import React from "react";

const NotFoundPage = (props) => {
    return (
        <div>
            <h1>404</h1>
            <p>Woops! Page not found. Your URL is invalid.</p>
            <img src="https://i.giphy.com/media/1rNWZu4QQqCUaq434T/giphy.webp" alt="This is fine."></img>
        </div>
    )
}

export default NotFoundPage;