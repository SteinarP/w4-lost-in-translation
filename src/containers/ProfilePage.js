import React from "react";
import { useParams} from "react-router-dom";
import { getStorage, clearStorage } from "../utils/storage";
import SpriteGallery from "../components/SpriteGallery";
import SpriteGalleryList from "../components/SpriteGalleryList";
import { useHistory } from "react-router-dom";

const ProfilePage = (props) => {
    const { id } = useParams();
    const history = useHistory();

    const recent = getStorage("recent");
    const arrayToList = (arr) => (
    <ol> 
        {arr.map((e,i) => <li key={i}>{e}</li>)} 
    </ol>)
    const recentList = recent? arrayToList(recent) : "No data";

    const handleLogOutClick = () =>{
        console.log("Logging out");
        clearStorage();
        history.push("/login");
    }

    return (
        <div>
            <h1>Profile: {getStorage("username")}</h1>            
            <button onClick={handleLogOutClick}>Clear and log out</button>         
            <p>Your recent translations:</p> 
            {recentList}
        </div>
    )
}

export default ProfilePage;